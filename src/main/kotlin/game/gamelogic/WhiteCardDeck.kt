package game.gamelogic

import model.WhiteCard

class WhiteCardDeck(cards: List<WhiteCard>, private val handSize: Int) {

    val userHands: Map<PlayerId, List<WhiteCard>> get() {
        val whitePlayed: MutableMap<PlayerId, List<WhiteCard>> = HashMap()
        _userHands.forEach { (playerId, hand) -> whitePlayed[playerId] = hand.filter { card -> !playedCardIds[playerId]!!.contains(card.id) } }
        return whitePlayed
    }

    val whitePlayed: Map<PlayerId, List<WhiteCard>> get() {
        val whitePlayed: MutableMap<PlayerId, List<WhiteCard>> = HashMap()
        playedCardIds.forEach { (playerId, cardIds) -> whitePlayed[playerId] = cardIds.map { cardId -> _userHands[playerId]!!.find { it.id == cardId }!! } }
        return whitePlayed
    }

    private val drawPile: MutableList<WhiteCard> = cards.toMutableList()
    private val discardPile: MutableList<WhiteCard> = ArrayList()

    private val playedCardIds: MutableMap<PlayerId, List<String>> = HashMap()
    private val _userHands: MutableMap<PlayerId, MutableList<WhiteCard>> = HashMap()

    init {
        drawPile.shuffle()
    }

    fun addUser(playerId: PlayerId) {
        if (_userHands[playerId] != null) {
            throw Exception("User is already in the game")
        }

        _userHands[playerId] = ArrayList()
        playedCardIds[playerId] = ArrayList()

        drawUserToFull(playerId)
    }

    fun removeUser(playerId: PlayerId) {
        if (_userHands[playerId] == null) {
            throw Exception("User has not been added")
        }

        discardPile.addAll(_userHands[playerId]!!)
        _userHands.remove(playerId)
        playedCardIds.remove(playerId)
    }

    fun playCards(playerId: PlayerId, cardIds: List<String>) {
        // TODO - Write UT that tries using duplicate card ids

        if (_userHands[playerId] == null) {
            throw Exception("User has not been added")
        } else if (!_userHands[playerId]!!.map { it.id }.containsAll(cardIds)) {
            throw Exception("User does not have all of those cards in their hand")
        } else if (playedCardIds[playerId]!!.isNotEmpty()) {
            throw Exception("User has already played")
        } else {
            playedCardIds[playerId] = cardIds.toList()
        }
    }

    fun unPlayCards(playerId: PlayerId) {
        when {
            _userHands[playerId] == null -> throw Exception("User has not been added")
            playedCardIds[playerId]!!.isEmpty() -> throw Exception("User has not played anything yet")
            else -> playedCardIds[playerId] = listOf()
        }
    }

    fun discardPlayedCardsAndRedraw() {
        _userHands.keys.forEach { playerId ->
            val cardsToRemove = _userHands[playerId]!!.filter { playedCardIds[playerId]!!.contains(it.id) }
            discardPile.addAll(cardsToRemove)
            _userHands[playerId]!!.removeAll(cardsToRemove)
            playedCardIds[playerId] = ArrayList()
        }

        drawAllUsersToFull()
    }

    fun revertPlayedCards() {
        playedCardIds.keys.forEach { playedCardIds[it] = ArrayList() }
    }

    fun resetAndDrawNewHands() {
        _userHands.keys.forEach { playerId ->
            discardPile.addAll(_userHands[playerId]!!)
            _userHands[playerId]!!.clear()
            playedCardIds[playerId] = ArrayList()
        }

        drawAllUsersToFull()
    }

    private fun shuffleDiscardPile() {
        drawPile.addAll(discardPile)
        discardPile.clear()
        drawPile.shuffle()
    }

    private fun popCardFromDrawPile(): WhiteCard {
        if (drawPile.isEmpty() && discardPile.isEmpty()) {
            throw Exception("No cards are left in the deck")
        }

        if (drawPile.isEmpty()) {
            shuffleDiscardPile()
        }

        return drawPile.removeAt(0)
    }

    private fun drawUserToFull(playerId: PlayerId) {
        val hand = _userHands[playerId]!!

        while (hand.size < handSize) {
            hand.add(popCardFromDrawPile())
        }
    }

    private fun drawAllUsersToFull() {
        _userHands.keys.forEach { drawUserToFull(it) }
    }
}