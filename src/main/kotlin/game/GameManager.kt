package game

import api.CardFetcher
import api.UserFetcher
import messageQueue.MessageQueue
import model.FOVGameData
import model.GameInfo
import java.io.Closeable
import java.lang.IllegalArgumentException
import java.util.Date
import java.util.Timer
import kotlin.concurrent.schedule

// TODO - Transition timer schedule to coroutine and use mutexes to provide concurrency safety
class GameManager(private val userFetcher: UserFetcher, private val cardFetcher: CardFetcher, recyclerOptions: GameManagerOptions, private val messageQueue: MessageQueue?) : Closeable {
    private val gamesByName: MutableMap<String, Game> = HashMap()
    private val gamesByUserId: MutableMap<String, Game> = HashMap()
    private val gameRecyclerTimer = Timer()

    init {
        if (recyclerOptions.inactiveGameTTLMillis != null) {
            gameRecyclerTimer.schedule(0, recyclerOptions.taskIntervalMillis) {
                synchronized(this) {
                    gamesByName.values.forEach {
                        if (Date().time - it.lastVote.time > recyclerOptions.inactiveGameTTLMillis) {
                            recycleGame(it.name)
                        }
                    }
                }
            }
        }
    }

    override fun close() {
        gameRecyclerTimer.cancel()
    }

    fun createGame(userId: String, gameName: String, maxPlayers: Int, maxScore: Int, handSize: Int, cardpackIds: List<String>): FOVGameData {
        if (gamesByName.containsKey(gameName)) {
            throw Exception("Game already exists with name: $gameName")
        }
        val cards = cardFetcher.getCards(cardpackIds)
        val game = Game(gameName, maxPlayers, maxScore, handSize, cards.first, cards.second, userFetcher)
        gamesByName[gameName] = game
        addUserToGame(userId, game)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
        messageQueue?.gameListUpdated()
        return getUserFOV(userId)!!
    }

    fun startGame(userId: String): FOVGameData {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.start(userId)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
        messageQueue?.gameListUpdated()
        return game.getFOV(userId)
    }

    fun stopGame(userId: String): FOVGameData {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.stop(userId)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
        messageQueue?.gameListUpdated()
        return game.getFOV(userId)
    }

    fun joinGame(userId: String, gameName: String): FOVGameData {
        val game = gamesByName[gameName] ?: throw Exception("Game does not exist with name: $gameName")
        addUserToGame(userId, game)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
        messageQueue?.gameListUpdated()
        return game.getFOV(userId)
    }

    fun leaveGame(userId: String) {
        val game = gamesByUserId[userId] // Guaranteed to be non-null if removeUserFromGame succeeds
        removeUserFromGame(userId)
        messageQueue?.gameUpdatedForUsers(game!!.playerIds + userId)
        messageQueue?.gameListUpdated()
    }

    fun addArtificialPlayer(userId: String, name: String): FOVGameData {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.addArtificialPlayer(userId, name)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
        return game.getFOV(userId)
    }

    fun removeArtificialPlayer(userId: String, name: String): FOVGameData {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.removeArtificialPlayer(userId, name)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
        return game.getFOV(userId)
    }

    fun addArtificialPlayers(userId: String, amount: Int): FOVGameData {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.addArtificialPlayers(userId, amount)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
        return game.getFOV(userId)
    }

    fun removeArtificialPlayers(userId: String, amount: Int): FOVGameData {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.removeArtificialPlayers(userId, amount)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
        return game.getFOV(userId)
    }

    fun kick(kickerId: String, kickeeId: String): FOVGameData {
        val kickerGame = gamesByUserId[kickerId] ?: throw Exception("Kicker is not in a game")
        val kickeeGame = gamesByUserId[kickeeId] ?: throw Exception("Kickee is not in a game")

        if (kickerGame != kickeeGame) {
            throw Exception("User is not in the same game")
        }

        kickerGame.kickUser(kickerId, kickeeId)
        gamesByUserId.remove(kickeeId)
        messageQueue?.gameUpdatedForUsers(kickerGame.playerIds + kickeeId)
        messageQueue?.gameListUpdated()
        return kickerGame.getFOV(kickerId)
    }

    fun ban(bannerId: String, banneeId: String): FOVGameData {
        val bannerGame = gamesByUserId[bannerId] ?: throw Exception("Banner is not in a game")
        val banneeGame = gamesByUserId[banneeId] ?: throw Exception("Bannee is not in a game")

        if (bannerGame != banneeGame) {
            throw Exception("User is not in the same game")
        }

        bannerGame.banUser(bannerId, banneeId)
        gamesByUserId.remove(banneeId)
        messageQueue?.gameUpdatedForUsers(bannerGame.playerIds + banneeId)
        messageQueue?.gameListUpdated()
        return bannerGame.getFOV(bannerId)
    }

    fun unban(unbannerId: String, unbanneeId: String): FOVGameData {
        val unbannerGame = gamesByUserId[unbannerId] ?: throw Exception("Banner is not in a game")
        unbannerGame.unbanUser(unbannerId, unbanneeId)
        messageQueue?.gameUpdatedForUsers(unbannerGame.playerIds)
        return unbannerGame.getFOV(unbannerId)
    }

    fun play(userId: String, cardIds: List<String>) {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.playCards(userId, cardIds)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
    }

    fun unPlay(userId: String) {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.unPlayCards(userId)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
    }

    fun vote(userId: String, cardId: String) {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.voteCard(userId, cardId)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
    }

    fun startNextRound(userId: String) {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.startNextRound()
        messageQueue?.gameUpdatedForUsers(game.playerIds)
    }

    fun sendMessage(userId: String, message: String) {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.addMessage(userId, message)
        messageQueue?.gameUpdatedForUsers(game.playerIds)
    }

    fun getUserFOV(userId: String): FOVGameData? {
        return gamesByUserId[userId]?.getFOV(userId)
    }

    fun getInfoList(): List<GameInfo> {
        return gamesByName.toList().sortedWith(compareBy { it.second.name }).map { game -> game.second.getInfo() }
    }

    private fun addUserToGame(userId: String, game: Game) {
        if (gamesByUserId[userId] != null) {
            if (gamesByUserId[userId] == game) {
                throw Exception("Cannot join a game you are already in")
            }
            removeUserFromGame(userId)
        }
        game.join(userId)
        gamesByUserId[userId] = game
    }

    private fun removeUserFromGame(userId: String) {
        val game = gamesByUserId[userId] ?: throw Exception("User is not in a game")
        game.leave(userId)
        gamesByUserId.remove(userId)
        if (game.isEmpty()) {
            gamesByName.remove(game.name)
        }
    }

    private fun recycleGame(gameName: String) {
        val game = gamesByName[gameName] ?: throw IllegalArgumentException("Game does not exist with this name")
        val playerIds = game.playerIds.toList() // Need to create non-referenced copy of player ID's so that we can send an event to the message queue containing them
        playerIds.forEach { removeUserFromGame(it) }
        messageQueue?.gameUpdatedForUsers(playerIds)
        messageQueue?.gameListUpdated()
    }

    data class GameManagerOptions(
        val taskIntervalMillis: Long,
        val inactiveGameTTLMillis: Long? // Setting to null prevents inactive games from being recycled
    )
}