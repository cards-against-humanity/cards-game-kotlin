package model

data class FOVArtificialPlayer(
    val name: String,
    val score: Int
)