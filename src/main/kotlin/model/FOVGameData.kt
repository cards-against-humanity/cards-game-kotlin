package model

import game.gamelogic.GameLogic
import game.Message

data class FOVGameData(
    val name: String,
    val maxPlayers: Int,
    val maxScore: Int,
    val stage: GameLogic.GameStage,
    val hand: List<WhiteCard>?,
    val players: List<FOVPlayer>,
    val artificialPlayers: List<FOVArtificialPlayer>,
    val queuedPlayers: List<FOVPlayer>,
    val queuedArtificialPlayers: List<FOVArtificialPlayer>,
    val bannedPlayers: List<User>,
    val judgeId: String?,
    val ownerId: String,
    val whitePlayed: List<WhitePlayedEntry>,
    val whitePlayedAnonymous: List<List<WhiteCard>>?,
    val currentBlackCard: BlackCard?,
    val winner: RealOrArtificialPlayer?,
    val messages: List<Message>,
    val pastRounds: List<PastRound>
)

data class RealOrArtificialPlayer(
    val user: User?,
    val artificialPlayerName: String?
)

data class PastRound(val blackCard: BlackCard, val whitePlayed: List<WhitePlayedEntry>, val judge: User, val winner: RealOrArtificialPlayer)

data class WhitePlayedEntry(val player: RealOrArtificialPlayer, val cards: List<WhiteCard?>)