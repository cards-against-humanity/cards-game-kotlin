package messageQueue

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import java.io.Closeable

class RabbitMessageQueue(uri: String) : MessageQueue, Closeable {
    // TODO - Safely close connection and channel if something breaks in init and implement auto-reconnect if the message queue ever goes down
    // TODO - Setup TLS (or confirm that it's already there)
    companion object {
        private const val GAME_QUEUE_NAME = "GAME"
        private const val GAME_LIST_UPDATE_MESSAGE = "{\"type\": \"GAME_LIST_UPDATED\"}"

        private fun constructGameUpdateMessage(userIds: List<String>): String {
            return "{\"type\": \"GAME_UPDATED\", \"payload\": ${userIds.map { "\"$it\"" }.joinToString(prefix = "[", postfix = "]", separator = ", ")}}"
        }
    }

    private val connection: Connection
    private val channel: Channel

    init {
        val connectionFactory = ConnectionFactory()
        connectionFactory.setUri(uri)
        connection = connectionFactory.newConnection()
        channel = connection.createChannel()
        channel.queueDeclare(GAME_QUEUE_NAME, false, false, false, null)
    }

    override fun gameUpdatedForUsers(userIds: List<String>) {
        if (userIds.isNotEmpty()) {
            channel.basicPublish("", GAME_QUEUE_NAME, null, constructGameUpdateMessage(userIds).toByteArray(Charsets.UTF_8))
        }
    }

    override fun gameListUpdated() {
        channel.basicPublish("", GAME_QUEUE_NAME, null, GAME_LIST_UPDATE_MESSAGE.toByteArray(Charsets.UTF_8))
    }

    override fun close() {
        connection.close()
        channel.close()
    }
}