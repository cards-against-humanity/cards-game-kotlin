package messageQueue

class MemoryMessageQueue : MessageQueue {
    private var gameUpdateCalls: MutableList<List<String>> = ArrayList()
    private var gameListUpdateCount = 0

    override fun gameUpdatedForUsers(userIds: List<String>) {
        gameUpdateCalls.add(userIds)
    }

    override fun gameListUpdated() {
        gameListUpdateCount++
    }

    fun getGameUpdateCalls(): List<List<String>> {
        return gameUpdateCalls
    }

    fun getGameListUpdateCount(): Int {
        return gameListUpdateCount
    }
}