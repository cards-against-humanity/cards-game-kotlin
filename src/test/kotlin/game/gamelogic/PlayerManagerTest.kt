package game.gamelogic

import model.WhiteCard
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNull
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class PlayerManagerTest {

    private val handSize = 4
    private val deckSize = 1000
    private var deck: WhiteCardDeck = WhiteCardDeck(listOf(), handSize)
    private var playerManager = PlayerManager(deck)

    @BeforeEach
    fun reset() {
        val cards = ArrayList<WhiteCard>()
        for (i in 1..deckSize) {
            cards.add(TestWhiteCard(i.toString(), "1", i.toString()))
        }
        deck = WhiteCardDeck(cards, handSize)
        playerManager = PlayerManager(deck)
    }

    @Test
    fun accessPlayerObject() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        val player = playerManager.players["1"]
        assertNotNull(player)
    }

    @Test
    fun incrementPlayerScore() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        val player = playerManager.players["1"]!!
        assertEquals(0, player.score)
        playerManager.incrementPlayerScore(PlayerId.fromUserId(player.id))
        assertEquals(1, player.score)
    }

    @Test
    fun resetScores() {
        val players: MutableList<PlayerGameLogicModel> = ArrayList()
        for (i in 1..100) {
            playerManager.addPlayer(PlayerId.fromUserId(i.toString()))
            val player = playerManager.players[i.toString()]!!
            for (j in 1..i) {
                playerManager.incrementPlayerScore(PlayerId.fromUserId(player.id))
            }
            players.add(player)
        }
        playerManager.resetScores()
        players.forEach { player ->
            assertEquals(0, player.score)
        }
    }

    @Test
    fun addDuplicateUser() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        val e = assertThrows(Exception::class.java, { playerManager.addPlayer(PlayerId.fromUserId("1")) })
        assertEquals("User is already in the game", e.message)
    }

    @Test
    fun removeUser() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        playerManager.removePlayer(PlayerId.fromUserId("1"))
        val player = playerManager.players["1"]
        assertNull(player)
    }

    @Test
    fun removeNonExistentUser() {
        val e = assertThrows(Exception::class.java, { playerManager.removePlayer(PlayerId.fromUserId("1")) })
        assertEquals("User is not in the game", e.message)
    }

    @Test
    fun ownerIsNullOnCreation() {
        assertNull(playerManager.owner)
    }

    @Test
    fun ownerIsAssignedToFirstUser() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        val owner = playerManager.owner
        assertNotNull(owner)
        assertEquals("1", owner!!.id)
    }

    @Test
    fun ownerIsUnassignedWhenAllUsersLeave() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        playerManager.removePlayer(PlayerId.fromUserId("1"))
        assertNull(playerManager.owner)
    }

    @Test
    fun ownerIsReassignedWhenUserLeaves() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        playerManager.addPlayer(PlayerId.fromUserId("2"))
        playerManager.addPlayer(PlayerId.fromUserId("3"))
        playerManager.addPlayer(PlayerId.fromUserId("4"))
        assertEquals("1", playerManager.owner!!.id)
        playerManager.removePlayer(PlayerId.fromUserId("1"))
        assertEquals("2", playerManager.owner!!.id)
        playerManager.removePlayer(PlayerId.fromUserId("2"))
        assertEquals("3", playerManager.owner!!.id)
        playerManager.removePlayer(PlayerId.fromUserId("3"))
        assertEquals("4", playerManager.owner!!.id)
    }

    @Test
    fun nullJudgeByDefault() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        assertNull(playerManager.judge)
    }

    @Test
    fun assignJudge() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        playerManager.nextJudge()
        assertNotNull(playerManager.judge)
        assertEquals("1", playerManager.judge!!.id)
    }

    @Test
    fun cycleThroughJudgesWithoutError() {
        for (i in 1..20) {
            playerManager.addPlayer(PlayerId.fromUserId(i.toString()))
        }
        for (i in 1..200) {
            playerManager.nextJudge()
            assertNotNull(playerManager.judge)
        }
    }

    @Test
    fun cycleThroughJudgesLinearly() {
        for (i in 1..20) {
            playerManager.addPlayer(PlayerId.fromUserId(i.toString()))
        }

        for (i in 1..10) {
            val judgeCounts: MutableMap<String, Int> = HashMap()
            for (j in 1..20) {
                playerManager.nextJudge()
                if (judgeCounts[playerManager.judge!!.id] == null) {
                    judgeCounts[playerManager.judge!!.id] = 1
                } else {
                    judgeCounts[playerManager.judge!!.id]!!.inc()
                }
            }

            for (j in 1..20) {
                assertEquals(1, judgeCounts[i.toString()])
            }
        }
    }

    @Test
    fun resetJudge() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        playerManager.nextJudge()
        playerManager.resetJudge()
        assertNull(playerManager.judge)
    }

    @Test
    fun reassignJudgeWhenCurrentJudgeLeaves() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        playerManager.addPlayer(PlayerId.fromUserId("2"))
        playerManager.addPlayer(PlayerId.fromUserId("3"))
        playerManager.addPlayer(PlayerId.fromUserId("4"))
        playerManager.nextJudge()
        val judgeId = playerManager.judge!!.id
        playerManager.removePlayer(PlayerId.fromUserId(judgeId))
        assertNotEquals(judgeId, playerManager.judge!!.id)
    }

    @Test
    fun removeJudgesOneAtATime() {
        for (i in 1..10) {
            playerManager.addPlayer(PlayerId.fromUserId(i.toString()))
        }
        playerManager.nextJudge()
        for (i in 1..9) {
            val judgeId = playerManager.judge!!.id
            playerManager.removePlayer(PlayerId.fromUserId(judgeId))
            assertNotEquals(judgeId, playerManager.judge!!.id)
        }
        playerManager.removePlayer(PlayerId.fromUserId(playerManager.judge!!.id))
        assertNull(playerManager.judge)
    }

    @Test
    fun addCardsToPlayerHands() {
        deck.addUser(PlayerId.fromUserId("1"))
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        assertEquals(handSize, playerManager.players["1"]!!.hand.size)
    }

    @Test
    fun discardPlayerHandWhenLeavingGame() {
        for (i in 1..(deckSize * 10)) {
            playerManager.addPlayer(PlayerId.fromUserId("1"))
            playerManager.removePlayer(PlayerId.fromUserId("1"))
        }
    }

    @Test
    fun addPlayersToMap() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        assertNotNull(playerManager.players["1"])
    }

    @Test
    fun addPlayersToList() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        assertEquals("1", playerManager.playersList[0].id)
    }

    @Test
    fun removePlayersFromMap() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        playerManager.removePlayer(PlayerId.fromUserId("1"))
        assertNull(playerManager.players["1"])
    }

    @Test
    fun removePlayersFromList() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        playerManager.removePlayer(PlayerId.fromUserId("1"))
        assertTrue(playerManager.playersList.isEmpty())
    }

    @Test
    fun queuedUserIdsAreCorrectInMap() {
        playerManager.addPlayerToQueue(PlayerId.fromUserId("1"))
        playerManager.addPlayerToQueue(PlayerId.fromUserId("2"))
        playerManager.addPlayerToQueue(PlayerId.fromUserId("3"))
        playerManager.addPlayerToQueue(PlayerId.fromUserId("4"))

        playerManager.queuedPlayers.forEach { t, u -> assertEquals(t, u.id) }
    }

    @Test
    fun cannotAddQueuedUserAfterNormalUserWithSameIdHasBeenAdded() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        val e = assertThrows(Exception::class.java) { playerManager.addPlayerToQueue(PlayerId.fromUserId("1")) }
        assertEquals("User is already in the game", e.message)
    }

    @Test
    fun cannotAddNormalUserAfterQueuedUserWithSameIdHasBeenAdded() {
        playerManager.addPlayerToQueue(PlayerId.fromUserId("1"))
        val e = assertThrows(Exception::class.java) { playerManager.addPlayer(PlayerId.fromUserId("1")) }
        assertEquals("User is already in the game", e.message)
    }

    @Test
    fun canMoveQueuedUsersToNormalUserList() {
        playerManager.addPlayerToQueue(PlayerId.fromUserId("1"))
        assertNotNull(playerManager.queuedPlayers["1"])
        assertNull(playerManager.players["1"])
        playerManager.addQueuedUsers()
        assertNull(playerManager.queuedPlayers["1"])
        assertNotNull(playerManager.players["1"])
    }

    @Test
    fun queuedUserCanLeave() {
        playerManager.addPlayerToQueue(PlayerId.fromUserId("1"))
        playerManager.removePlayer(PlayerId.fromUserId("1"))
        assertNull(playerManager.players["1"])
        assertNull(playerManager.queuedPlayers["1"])
    }

    @Test
    fun normalUserCanLeave() {
        playerManager.addPlayer(PlayerId.fromUserId("1"))
        playerManager.removePlayer(PlayerId.fromUserId("1"))
        assertNull(playerManager.players["1"])
        assertNull(playerManager.queuedPlayers["1"])
    }

    private class TestWhiteCard(
        override val id: String,
        override val cardpackId: String,
        override val text: String
    ) : WhiteCard
}